#!/bin/bash

# Navigate to the Downloads directory
cd ~/Downloads

# Find the latest downloaded discord .deb file
file=$(ls -t ~/Downloads/discord-*.deb | head -1)

# Install the .deb file
pkexec dpkg -i $file &&

# Delete the .deb file
rm $file

# Open Discord
nohup discord > /dev/null 2>&1 &

# Discord Auto-Updater

This project contains scripts to automatically update Discord when a new .deb file is downloaded.

## Prerequisites

This script uses `inotifywait` to wait for a new file to be created in the `~/Téléchargements` directory. `inotifywait`
is part of the `inotify-tools` package, which is not installed by default on Ubuntu. You can install it with:

```bash
sudo apt-get install inotify-tools
```

sudo apt-get install inotify-tools

## Scripts

- `watch_downloads.sh: This script watches the ~/Téléchargements directory for new Discord .deb files. When a new file
  is detected, it runs the update_discord.sh script.

- `update_discord.sh: This script installs the latest downloaded Discord .deb file and then deletes the file. It then
  starts Discord. It prompts for the sudo password in the graphical dialog.

## Usage

1. Start the watch_downloads.sh script with the command sudo ```bash./watch_downloads.sh & ``` This will start the script in the
   background.

2. Download a new Discord .deb file into the ~/Téléchargements directory. The watch_downloads.sh script will detect the
   new file and run the update_discord.sh script, which will install the new version of Discord and then start the app.

## Auto-Start

To have the watch_downloads.sh script start automatically when you log in, add it to your Startup Applications with the
command /path/to/your/scripts/watch_downloads.sh &, replacing /path/to/your/scripts/ with the actual path to the
scripts.

## Troubleshooting

If the update_discord.sh script doesn't run as expected, make sure it's in the correct location (
/path/to/your/scripts/update_discord.sh) and that it's executable. You can make it executable with the command ```bash sudo chmod +x
/path/to/your/scripts/update_discord.sh ``` .

And Make sure both scripts (update_discord.sh and watch_downloads.sh) are executable. You can do this by running: ```bash sudo chmod
+x /path/to/your/scripts/update_discord.sh  && chmod +x /path/to/your/scripts/watch_downloads.sh``` 

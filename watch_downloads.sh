#!/bin/bash

# Directory to watch
dir=~/Downloads

# Command to run when a Discord .deb file is downloaded
command=~/scripts/update_discord.sh

# Use inotifywait in a loop to watch for changes to the directory
while inotifywait -q -e create "$dir"; do
    # If a Discord .deb file with version numbers was created, run the command
    if ls "$dir"/discord-[0-9]*.[0-9]*.[0-9]*.deb > /dev/null 2>&1; then
        $command
    fi
done
